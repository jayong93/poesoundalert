import sys
import main_window_form
import cooltime_helper_module
import os.path
import json
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot


config_path = "~/.CooltimeAlertConfig.json"


class MainForm(QtWidgets.QDialog, main_window_form.Ui_Dialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent, QtCore.Qt.WindowTitleHint |
                                   QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowCloseButtonHint)

        self.setupUi(self)
        cooltime_helper_module.init_module()

        self.combo_modifier_list.addItems(
            cooltime_helper_module.modifier_list())
        self.combo_key_list.addItems(cooltime_helper_module.key_list())
        self.combo_condition.addItems(cooltime_helper_module.condition_list())

        self.button_add.clicked.connect(self.add_key)
        self.button_del.clicked.connect(self.del_key)
        self.button_find_file.clicked.connect(self.open_file_dialog)
        self.button_save.clicked.connect(self.save_config)

        self.load_config()

        self.show()

    @pyqtSlot()
    def del_key(self):
        selected_items = self.table_hot_key.selectedItems()
        item_data = [item for item in selected_items if item.column() == 0]
        item_names = [item.text() for item in item_data]
        print(item_names)
        cooltime_helper_module.del_key(item_names)
        for item in item_data:
            self.table_hot_key.removeRow(item.row())

    @pyqtSlot()
    def add_key(self):
        name = self.line_name.text()
        if len(name) == 0:
            return
        modifier = self.combo_modifier_list.currentText()
        key = self.combo_key_list.currentText()
        condition = self.combo_condition.currentText()
        time = self.spin_time.value()
        file_name = self.line_file_name.text()
        if len(file_name) == 0:
            return

        self.inner_add_key(name, modifier, key, condition, time, file_name)

    def inner_add_key(self, name, modifier, key, condition, time, file_name):
        is_added = cooltime_helper_module.add_key(
            name, modifier, key, condition, time, file_name)
        if is_added:
            new_row = self.table_hot_key.rowCount()
            self.table_hot_key.insertRow(new_row)
            self.table_hot_key.setItem(
                new_row, 0, QtWidgets.QTableWidgetItem(name))
            self.table_hot_key.setItem(
                new_row, 1, QtWidgets.QTableWidgetItem(modifier))
            self.table_hot_key.setItem(
                new_row, 2, QtWidgets.QTableWidgetItem(key))
            self.table_hot_key.setItem(
                new_row, 3, QtWidgets.QTableWidgetItem(condition))
            self.table_hot_key.setItem(
                new_row, 4, QtWidgets.QTableWidgetItem(str(time)))
            self.table_hot_key.setItem(
                new_row, 5, QtWidgets.QTableWidgetItem(file_name))

    @pyqtSlot()
    def open_file_dialog(self):
        dialog = QtWidgets.QFileDialog(self)
        dialog.setNameFilter("Sound Files (*.mp3 *.wav *.ogg)")
        dialog.open(lambda: self.sound_file_selected(dialog))

    @pyqtSlot()
    def save_config(self):
        row_count = self.table_hot_key.rowCount()
        total_configs = []
        for row in range(0, row_count):
            total_configs.append(
                {
                    "name": self.table_hot_key.item(row, 0).text(),
                    "modifier": self.table_hot_key.item(row, 1).text(),
                    "key": self.table_hot_key.item(row, 2).text(),
                    "condition": self.table_hot_key.item(row, 3).text(),
                    "time": float(self.table_hot_key.item(row, 4).text()),
                    "sound": self.table_hot_key.item(row, 5).text(),
                }
            )
        with open(os.path.expanduser(config_path), 'w') as f:
            json.dump(total_configs, f, ensure_ascii=False)

    def load_config(self):
        if os.path.isfile(os.path.expanduser(config_path)):
            with open(os.path.expanduser(config_path)) as f:
                configs = json.load(f)
                for hotkey in configs:
                    self.inner_add_key(hotkey["name"], hotkey["modifier"], hotkey["key"],
                                       hotkey["condition"], hotkey["time"], hotkey["sound"])

    def sound_file_selected(self, dialog):
        result = dialog.selectedFiles()
        if len(result) == 0:
            return

        self.line_file_name.setText(result[0])


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainForm()
    sys.exit(app.exec())
