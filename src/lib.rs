use lazy_static::lazy_static;
use pyo3::prelude::*;
use std::char::from_u32_unchecked;
use std::collections::HashMap;
use std::fs::File;
use std::io::Cursor;
use std::sync::{
    mpsc::{self, Receiver, Sender},
    Mutex,
};
use std::thread;

const AFTER_COND: &str = "누르고 X 초 뒤에 재생";
const WHILE_COND: &str = "X 초 동안 누른 뒤에 재생";

#[derive(Clone)]
enum InternalMessage {
    AddKey {
        name: String,
        modifier: i32,
        key: i32,
        condition: Condition,
        time: f64,
        file: String,
        responser: Sender<bool>,
    },
    DelKey(Vec<String>),
    Play(String),
}

enum TimerMessage {
    Add {
        name: String,
        modifier: i32,
        key: i32,
        time: f64,
        condition: Condition,
    },
    Remove(Vec<String>),
}

lazy_static! {
    static ref MODIFIERS: HashMap<String, i32> = {
        use winapi::um::winuser::*;
        let mut map = HashMap::new();
        map.insert("None".to_string(), 0);
        map.insert("Alt".to_string(), VK_MENU);
        map.insert("Ctrl".to_string(), VK_CONTROL);
        map.insert("Shift".to_string(), VK_SHIFT);
        map
    };
    static ref KEYS: HashMap<String, i32> = {
        use winapi::um::winuser::*;
        let mut map = HashMap::new();
        for i in 1..13 {
            map.insert(format!("F{}", i), VK_F1 + (i - 1));
        }
        for i in 0..10 {
            map.insert(format!("{}", i), '0' as i32 + i);
        }
        for i in ('A' as i32)..=('Z' as i32) {
            map.insert(format!("{}", unsafe { from_u32_unchecked(i as u32) }), i);
        }
        map.insert("MOUSE_LBUTTON".to_string(), VK_LBUTTON);
        map.insert("MOUSE_RBUTTON".to_string(), VK_RBUTTON);
        map.insert("MOUSE_MBUTTON".to_string(), VK_MBUTTON);
        map.insert("MOUSE_BUTTON3".to_string(), VK_XBUTTON1);
        map.insert("MOUSE_BUTTON4".to_string(), VK_XBUTTON2);
        map.insert("SPACE".to_string(), VK_SPACE);
        map.insert("INSERT".to_string(), VK_INSERT);
        map.insert("DELETE".to_string(), VK_DELETE);
        map.insert("HOME".to_string(), VK_HOME);
        map.insert("END".to_string(), VK_END);
        map.insert("PAGE_UP".to_string(), VK_PRIOR);
        map.insert("PAGE_DOWN".to_string(), VK_NEXT);
        map
    };
    static ref SENDER_TO_INNER_THREAD: Mutex<Option<Sender<InternalMessage>>> = Mutex::new(None);
    static ref SENDER_TO_TIMER_THREAD: Mutex<Option<Sender<TimerMessage>>> = Mutex::new(None);
    static ref DEFAULT_SOUND_DEVICE: rodio::Device = rodio::default_output_device().unwrap();
}

#[derive(Clone, Copy, Debug)]
enum Condition {
    After,
    While,
}

fn key_check_thread_func(
    timer_sender: Sender<TimerMessage>,
    receiver: Receiver<InternalMessage>,
) -> impl FnOnce() -> () {
    use std::io::Read;
    move || {
        let mut registered_map = HashMap::new();

        for msg in receiver.iter() {
            match msg {
                InternalMessage::AddKey {
                    name,
                    modifier,
                    key,
                    responser,
                    time,
                    condition,
                    file,
                } => {
                    if registered_map.contains_key(&name) {
                        responser.send(false).unwrap();
                    } else {
                        if let Ok(mut f) = File::open(file) {
                            let mut data = vec![];
                            if f.read_to_end(&mut data).is_err() {
                                responser.send(false).unwrap();
                            } else {
                                registered_map.insert(name.clone(), Cursor::new(data));
                                timer_sender
                                    .send(TimerMessage::Add {
                                        name,
                                        modifier,
                                        key,
                                        time,
                                        condition,
                                    })
                                    .ok();
                                responser.send(true).unwrap();
                            }
                        } else {
                            responser.send(false).unwrap();
                        }
                    }
                }
                InternalMessage::DelKey(names) => {
                    timer_sender.send(TimerMessage::Remove(names.clone())).ok();
                    names.into_iter().for_each(|name| {
                        registered_map.remove(&name);
                    });
                }
                InternalMessage::Play(name) => {
                    let sound = registered_map.get(&name).unwrap();
                    let decoder = rodio::Decoder::new(sound.clone()).unwrap();
                    let sink = rodio::Sink::new(&DEFAULT_SOUND_DEVICE);
                    sink.append(decoder);
                    sink.detach();
                }
            }
        }
    }
}

struct HotKey {
    modifier: i32,
    key: i32,
    time: f64,
    condition: Condition,
    remain_time: Option<f64>,
    is_pressed: bool,
}

fn decrease_key_count(key_count_map: &mut HashMap<i32, (usize, bool)>, key: i32) {
    let new_count = key_count_map.get_mut(&key).map(|(count, _)| {
        *count -= 1;
        *count
    });
    match new_count {
        Some(count) if count == 0 => {
            key_count_map.remove(&key);
        }
        _ => {}
    }
}

fn timer_thread_func(
    inner_sender: Sender<InternalMessage>,
    receiver: Receiver<TimerMessage>,
) -> impl FnOnce() -> () {
    use std::time::Instant;
    use winapi::um::winuser::GetKeyState;
    move || {
        let mut key_map = HashMap::<String, HotKey>::new();
        let mut key_count_map = HashMap::<i32, (usize, bool)>::new();
        let mut start = Instant::now();
        loop {
            let new_start = Instant::now();
            let elapsed_time = new_start.duration_since(start).as_secs_f64();
            start = new_start;

            // key state update
            for (key, (_, is_pressed)) in key_count_map.iter_mut() {
                unsafe {
                    let state = GetKeyState(*key);
                    *is_pressed = state as u16 >> 15 != 0;
                }
            }

            for (name, hotkey) in key_map.iter_mut() {
                let is_both_pressed = {
                    key_count_map
                        .get(&hotkey.modifier)
                        .map(|(_, is_pressed)| *is_pressed)
                        .unwrap_or(true)
                        && key_count_map
                            .get(&hotkey.key)
                            .map(|(_, is_pressed)| *is_pressed)
                            .unwrap()
                };
                match (is_both_pressed, hotkey.is_pressed, hotkey.condition) {
                    (true, false, _) => {
                        hotkey.remain_time = Some(hotkey.time);
                        hotkey.is_pressed = true;
                    }
                    (true, true, _) | (false, false, Condition::After) => {
                        hotkey.remain_time.as_mut().map(|t| *t -= elapsed_time);
                    }
                    (false, true, Condition::After) => {
                        hotkey.is_pressed = false;
                    }
                    (false, true, Condition::While) => {
                        hotkey.remain_time = None;
                        hotkey.is_pressed = false;
                    }
                    _ => {}
                }

                if let Some(remain_time) = hotkey.remain_time {
                    if remain_time <= 0.0 {
                        hotkey.remain_time = None;
                        inner_sender.send(InternalMessage::Play(name.clone())).ok();
                    }
                }
            }

            for msg in receiver.try_iter() {
                match msg {
                    TimerMessage::Add {
                        name,
                        modifier,
                        key,
                        time,
                        condition,
                    } => {
                        let old_val = key_map.insert(
                            name,
                            HotKey {
                                modifier,
                                key,
                                time,
                                condition,
                                remain_time: None,
                                is_pressed: false,
                            },
                        );
                        match old_val {
                            Some(hotkey) => {
                                decrease_key_count(&mut key_count_map, hotkey.key);
                                if hotkey.modifier != 0 {
                                    decrease_key_count(&mut key_count_map, hotkey.modifier);
                                }
                            }
                            _ => {}
                        }
                        key_count_map.entry(key).or_insert((0, false)).0 += 1;
                        if modifier != 0 {
                            key_count_map.entry(modifier).or_insert((0, false)).0 += 1;
                        }
                    }
                    TimerMessage::Remove(names) => {
                        for name in names {
                            key_map.remove(&name).map(|hotkey| {
                                if hotkey.modifier != 0 {
                                    decrease_key_count(&mut key_count_map, hotkey.modifier);
                                }
                                decrease_key_count(&mut key_count_map, hotkey.key);
                            });
                        }
                    }
                }
            }

            thread::yield_now();
        }
    }
}

#[pyfunction]
fn init_module() {
    let (sender, receiver) = mpsc::channel();
    {
        let mut lg = SENDER_TO_INNER_THREAD.lock().unwrap();
        *lg = Some(sender.clone());
    }
    let (timer_sender, timer_receiver) = mpsc::channel();
    {
        let mut lg = SENDER_TO_TIMER_THREAD.lock().unwrap();
        *lg = Some(timer_sender.clone());
    }
    thread::spawn(timer_thread_func(sender, timer_receiver));
    thread::spawn(key_check_thread_func(timer_sender, receiver));
}

#[pyfunction]
fn modifier_list() -> Vec<String> {
    let mut modifiers: Vec<_> = MODIFIERS.keys().cloned().collect();
    modifiers.sort_unstable();
    modifiers
}

#[pyfunction]
fn key_list() -> Vec<String> {
    let mut keys: Vec<String> = KEYS.keys().cloned().collect();
    keys.sort_unstable();
    keys
}

#[pyfunction]
fn condition_list() -> Vec<String> {
    vec![AFTER_COND.to_string(), WHILE_COND.to_string()]
}

fn translate_condition(condition: &str) -> Condition {
    match condition {
        AFTER_COND => Condition::After,
        WHILE_COND => Condition::While,
        _ => unreachable!("Unknown condition has been passed"),
    }
}

use std::fmt::Display;

fn make_py_error<T: Display>(msg: T) -> PyErr {
    PyErr::new::<pyo3::exceptions::RuntimeError, _>(format!("{}", msg))
}

#[pyfunction]
fn add_key(
    name: String,
    modifier: String,
    key: String,
    condition: String,
    time: f64,
    file_name: String,
) -> PyResult<bool> {
    let sender;
    {
        let lg = SENDER_TO_INNER_THREAD.lock().unwrap();
        sender = lg.as_ref().cloned().unwrap();
    }

    let (res_send, res_recv) = mpsc::channel();
    sender
        .send(InternalMessage::AddKey {
            modifier: MODIFIERS.get(&modifier).unwrap().clone(),
            key: KEYS.get(&key).unwrap().clone(),
            condition: translate_condition(&condition),
            time,
            file: file_name,
            name,
            responser: res_send,
        })
        .map_err(make_py_error)?;

    res_recv.recv().map_err(make_py_error)
}

#[pyfunction]
fn del_key(names: Vec<String>) {
    let sender;
    {
        let lg = SENDER_TO_INNER_THREAD.lock().unwrap();
        sender = lg.as_ref().cloned().unwrap();
    }

    sender.send(InternalMessage::DelKey(names)).ok();
}

#[pymodule]
fn cooltime_helper_module(_py: Python, m: &PyModule) -> PyResult<()> {
    use pyo3::wrap_pyfunction;
    m.add_wrapped(wrap_pyfunction!(init_module))?;
    m.add_wrapped(wrap_pyfunction!(add_key))?;
    m.add_wrapped(wrap_pyfunction!(del_key))?;
    m.add_wrapped(wrap_pyfunction!(condition_list))?;
    m.add_wrapped(wrap_pyfunction!(modifier_list))?;
    m.add_wrapped(wrap_pyfunction!(key_list))
}
